package com.aaronhible.mutual.server.upload.service;

import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;

@Service
public class FileSystemStorageService implements StorageService {

    private final Path rootLocation;

    @Autowired
    public FileSystemStorageService(final StorageProperties storageProperties) {
        this.rootLocation = Paths.get(storageProperties.getLocation());
    }

    @Override
    public void store(final MultipartFile file) {
        if (file.isEmpty()) {
            throw new StorageException("Failed to store empty file.");
        }

        // TODO: refactor these to functions
        final Path destinationFile = buildDestinationPath(file, this.rootLocation);
        checkPathLocations(destinationFile, this.rootLocation);

        try(InputStream inputStream = file.getInputStream()){
            Files.copy(inputStream, destinationFile, StandardCopyOption.REPLACE_EXISTING);
        } catch (IOException e) {
            throw new StorageException("Failed to store file.", e);
        }
    }

    private void checkPathLocations(final Path destinationFile, final Path rootLocation) {
        if(!destinationFile.getParent().equals(rootLocation.toAbsolutePath())) {
            throw new StorageException("Cannot store file outside current directory.");
        }
    }

    private Path buildDestinationPath(final MultipartFile file, final Path rootLocation) {
        final Path originalPath = Paths.get(file.getOriginalFilename());
        final Path resolvePath = rootLocation.resolve(originalPath);
        return resolvePath.normalize().toAbsolutePath();
    }
}
