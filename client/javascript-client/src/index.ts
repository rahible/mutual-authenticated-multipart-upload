import * as fs from 'fs';
import * as https from 'https';
import {
  default as axios
} from 'axios';
import FormData from 'form-data';

interface MultipartConfig {
  clientKeyFilePath: string;
  clientKeyPassword: string;
  destinationUrl: string;
  fileToSend: string;
}

class MultiPartUpload {
  
  constructor(private multipartConfig: MultipartConfig) {}

  getFormData(): FormData {
    const formData = new FormData();
    formData.append('file', fs.createReadStream(this.multipartConfig.fileToSend));
    return formData;
  }  
  
  getHttpsAgent(): https.Agent {
    // TLS configuration
    const key = fs.readFileSync(this.multipartConfig.clientKeyFilePath);
    const passphrase = this.multipartConfig.clientKeyPassword;
    // we are using self signed certificates, do not use in production
    const rejectUnauthorized = false;
    return new https.Agent({
      pfx: key,
      passphrase,
      rejectUnauthorized
    });
  }
  
  async post() {
    const formData = this.getFormData();
    const httpsAgent = this.getHttpsAgent();
    return axios.post(this.multipartConfig.destinationUrl, formData, { httpsAgent });
  }
  
}

export async function postFile() {
  // these values hsould be externalized
  const multipartConfig = {
    clientKeyFilePath: '../../certs/client/client.p12',
    clientKeyPassword: 'trustme',
    fileToSend: 'file/test-javascript.txt',
    destinationUrl: 'https://localhost:8080/upload'
  } as MultipartConfig;
  
  try {
    const multipartUpload = new MultiPartUpload(multipartConfig);
    const { data } = await multipartUpload.post();
    console.log(`successful: ${data}`);
  } catch (error) {
    console.log(`error: ${error}`);
  }
}

postFile();