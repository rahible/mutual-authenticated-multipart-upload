# Mutual Authenticated Multipart Upload

This project is built as a proof of concept of implementing mutually authenticated multipart file uploads using a client built in TypeScript. The server setup is important, but the server itself is not. The POC is to flush out how to do the client call in typescript.

This code is based on these two examples:

https://medium.com/@salarai.de/how-to-enable-mutual-tls-in-a-spring-boot-application-77144047940f
https://levelup.gitconnected.com/upload-and-stream-multipart-content-with-nodejs-in5minutes-61836e061080

The TypeScript/JavaScript client is using the axiom library for https and multipart upload. 

https:///github.com/axios/axios#using-multipartform-data-format

Table of Contents
- [Assumptions](#assumptions)
- [Prerequisites](#prerequisites)
- [How to Execute](#how-to-execute)
- [Project Layout](#project-layout)

## Assumptions

This project is a POC answering the question, "How to use TypesScript to upload a file using multipart requests and mutual TLS authentication". This is not a tutorial on how to do this, so there are assumptions on what you should know before executing this project.

* That the reader knows how to install or already has installed Java, Node and OpenSSL ([see Prerequisites](#prerequisites)).
* The reader has some understanding of Java, TypeScript and Mutual Authentication via TLS.

## Prerequisites

### Installed Tools

1. Java 21 or greater installed (java --version).
2. Node 21.60 or greater installed (node --version).
3. OpenSSL 1.1.1m  14 Dec 2021 installed(openssl version).

### Generating Certificates

I utilized the post below for generating the needed files for handling self-signed mutual authentication. In production this would not be used, as the keys would be provided through the certificate authority in the company.

When going through the tutorial ensure that the password "trustme" is used. This will make sure that the passwords align with what embedded tomcat is expecting.

https://bsaunder.github.io/security/server/ssl/tls/2012/07/03/generating-self-signed-certificates-for-mutual-authentication-in-java

Since this is a proof of concept, the values are hard coded. In a production application the passphrases, keys and certificates would be in a trusted store and accessible to only those that need it.

After generating the certificates. Copy them to the following areas.

certs/client/client.p12
client/java-client/src/main/resources/client.jks
server/src/main/resources/server.jks
server/src/main/resources/server_truststore.jks

# server/src/main/resources/application.yml

5  server:
6    ssl:
7      enabled: true
8      key-store: "classpath:server.jks"
9      key-store-password: trustme
10     key-store-type: PKCS12
11     client-auth: need
12     trust-store: "classpath:server_truststore.jks"
13     trust-store-password: trustme
14     trust-store-type: PKCS12
```

## How to Execute
Open 2 commandline windows (e.g. gitbash)

1. Open the first window and navigate to the server directory then execute "mvn spring-boot:run". If there are any errors around SSL/TLS or the certs configured then [see Prerequisites](#prerequisites).
2. Open the second window and navigate to the client/javascript-client directory and execute "npm i && npx tsc && npm run index".

The client will read the file ./client/javascript-client/file/test-javascript.txt then upload it to the spring boot server. Spring boot will then write the file to ./upload-dir/test-javascript.txt.

## Project Layout

project root
    |
    -- certs
          |
          client: copy client cert files here
    -- client
          |
          -- javascript-client: TypeScript client project base
          |
          -- java-client: Java client base (used to test the server before TypeScript was created)
    --  server
          -- spring boot server
          