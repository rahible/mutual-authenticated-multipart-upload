package com.aaronhible.mutual.server.upload.service;

import lombok.Getter;
import org.springframework.stereotype.Component;

@Getter
@Component
public class StorageProperties {
    private final String location = "upload-dir";
}
