package com.aaronhible.client;

import java.io.IOException;
import java.io.InputStream;
import java.security.KeyManagementException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.UnrecoverableKeyException;
import java.security.cert.CertificateException;

import javax.net.ssl.SSLContext;

import org.apache.hc.client5.http.classic.methods.HttpGet;
import org.apache.hc.client5.http.impl.classic.CloseableHttpClient;
import org.apache.hc.client5.http.impl.classic.HttpClients;
import org.apache.hc.client5.http.impl.io.PoolingHttpClientConnectionManager;
import org.apache.hc.client5.http.io.HttpClientConnectionManager;
import org.apache.hc.client5.http.socket.ConnectionSocketFactory;
import org.apache.hc.client5.http.ssl.SSLConnectionSocketFactory;
import org.apache.hc.client5.http.ssl.TrustSelfSignedStrategy;
import org.apache.hc.core5.http.config.Registry;
import org.apache.hc.core5.http.config.RegistryBuilder;
import org.apache.hc.core5.http.io.entity.EntityUtils;
import org.apache.hc.core5.http.message.StatusLine;
import org.apache.hc.core5.ssl.SSLContexts;

/**
 * Hello world!
 *
 */
public class GetMethodMain 
{
    public static void main( String[] args )
    {
        final String url = "https://localhost:8080/api";
        final String keyPassword = "trustme";
        final String certificate = "client.jks";
        
          KeyStore keyStore = null;
          InputStream instream = null;
          
          try {
            keyStore = KeyStore.getInstance("JKS");
            instream = GetMethodMain.class.getClassLoader().getResourceAsStream(certificate);
            keyStore.load(instream, keyPassword.toCharArray());
          } catch (CertificateException |
              KeyStoreException |
              IOException | 
              NoSuchAlgorithmException ex) {
            throw new RuntimeException(ex);
          } finally {
            try {
              instream.close();
            } catch (IOException e) {
              throw new RuntimeException(e);
            }
          }
        
          SSLContext sslContext = null;
          try {
            sslContext = SSLContexts.custom()
                            .loadKeyMaterial(keyStore, keyPassword.toCharArray())
                            .loadTrustMaterial(new TrustSelfSignedStrategy())
                            .build();
          } catch (NoSuchAlgorithmException |
                KeyManagementException |
                KeyStoreException |
                UnrecoverableKeyException ex) {
            throw new RuntimeException(ex);
          }
          
         SSLConnectionSocketFactory socketFactory = new SSLConnectionSocketFactory(sslContext);
         Registry<ConnectionSocketFactory> reg = 
                        RegistryBuilder.<ConnectionSocketFactory>create()
                                        .register("https", socketFactory)
                                        .build();
         HttpClientConnectionManager cm = new PoolingHttpClientConnectionManager(reg);
         CloseableHttpClient httpClient = HttpClients.custom()
                         .setConnectionManager(cm)
                         .build();
         HttpGet httpGet = new HttpGet(url);
         
         try {
           httpClient.execute(httpGet, response -> {
             System.out.println("------------------------------------");
             System.out.println(httpGet + "->" + new StatusLine(response));
             System.out.println(EntityUtils.toString(response.getEntity()));
             return null;
           });
         } catch (IOException e) {
           throw new RuntimeException(e);
         }
        
    }
}
