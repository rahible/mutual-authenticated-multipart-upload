package com.aaronhible.client;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URISyntaxException;
import java.net.URL;
import java.security.KeyManagementException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.UnrecoverableKeyException;
import java.security.cert.CertificateException;

import javax.net.ssl.SSLContext;

import org.apache.hc.client5.http.classic.methods.HttpPost;
import org.apache.hc.client5.http.entity.mime.HttpMultipartMode;
import org.apache.hc.client5.http.entity.mime.MultipartEntityBuilder;
import org.apache.hc.client5.http.impl.classic.CloseableHttpClient;
import org.apache.hc.client5.http.impl.classic.HttpClients;
import org.apache.hc.client5.http.impl.io.PoolingHttpClientConnectionManager;
import org.apache.hc.client5.http.io.HttpClientConnectionManager;
import org.apache.hc.client5.http.socket.ConnectionSocketFactory;
import org.apache.hc.client5.http.ssl.SSLConnectionSocketFactory;
import org.apache.hc.client5.http.ssl.TrustSelfSignedStrategy;
import org.apache.hc.core5.http.ContentType;
import org.apache.hc.core5.http.HttpEntity;
import org.apache.hc.core5.http.config.Registry;
import org.apache.hc.core5.http.config.RegistryBuilder;
import org.apache.hc.core5.http.io.entity.EntityUtils;
import org.apache.hc.core5.http.message.StatusLine;
import org.apache.hc.core5.ssl.SSLContexts;

/**
 * Hello world!
 *
 */
public class PostFileMain 
{
    public static void main( String[] args )
    {
        final String url = "https://localhost:8080/upload";
        final String keyPassword = "trustme";
        final String certificate = "client.p12";
        final String fileToUpload = "/test-java.txt";
        
          KeyStore keyStore = null;
          InputStream instream = null;
          
          try {
            keyStore = KeyStore.getInstance("JKS");
            instream = GetMethodMain.class.getClassLoader().getResourceAsStream(certificate);
            keyStore.load(instream, keyPassword.toCharArray());
          } catch (CertificateException |
              KeyStoreException |
              IOException | 
              NoSuchAlgorithmException ex) {
            throw new RuntimeException(ex);
          } finally {
            try {
              instream.close();
            } catch (IOException e) {
              throw new RuntimeException(e);
            }
          }
        
          SSLContext sslContext = null;
          try {
            sslContext = SSLContexts.custom()
                            .loadKeyMaterial(keyStore, keyPassword.toCharArray())
                            .loadTrustMaterial(new TrustSelfSignedStrategy())
                            .build();
          } catch (NoSuchAlgorithmException |
                KeyManagementException |
                KeyStoreException |
                UnrecoverableKeyException ex) {
            throw new RuntimeException(ex);
          }
          
         SSLConnectionSocketFactory socketFactory = new SSLConnectionSocketFactory(sslContext);
         Registry<ConnectionSocketFactory> reg = 
                        RegistryBuilder.<ConnectionSocketFactory>create()
                                        .register("https", socketFactory)
                                        .build();
         HttpClientConnectionManager cm = new PoolingHttpClientConnectionManager(reg);
         CloseableHttpClient httpClient = HttpClients.custom()
                         .setConnectionManager(cm)
                         .build();
         
         File file = null;
         try{
          URL fileUrl = PostFileMain.class.getResource(fileToUpload);                
          file = new File(fileUrl.toURI());
         } catch(URISyntaxException ue) {
          throw new RuntimeException("Could not load file:" + fileToUpload);
         }                
         MultipartEntityBuilder entityBuilder = MultipartEntityBuilder.create();
         entityBuilder.setMode(HttpMultipartMode.LEGACY);
         entityBuilder.addBinaryBody("file", file, ContentType.DEFAULT_BINARY, file.getName());
          final HttpEntity entity = entityBuilder.build();
          
          HttpPost httpPost = new HttpPost(url);
          httpPost.setEntity(entity);
         
         try {
           httpClient.execute(httpPost, response -> {
             System.out.println("------------------------------------");
             System.out.println(httpPost + "->" + new StatusLine(response));
             System.out.println(EntityUtils.toString(response.getEntity()));
             return null;
           });
         } catch (IOException e) {
           throw new RuntimeException(e);
         }
        
    }
}
